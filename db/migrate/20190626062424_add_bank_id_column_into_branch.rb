class AddBankIdColumnIntoBranch < ActiveRecord::Migration[5.2]
  def change
    add_column :branches, :bank_id, :integer
  end
end
