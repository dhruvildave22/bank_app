class CreateBranches < ActiveRecord::Migration[5.2]
  def change
    create_table :branches do |t|
      t.string :branch_code
      t.string :location
      t.string :ifsc_code
      t.integer :phone_no
      t.timestamps
    end
  end
end
