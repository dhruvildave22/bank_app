class AddReferenceInEachTable < ActiveRecord::Migration[5.2]
  def change
    add_reference :branches, :bank, foreign_key: true
    add_reference :accounts, :branch, foreign_key: true
    add_reference :accounts, :user, foreign_key: true
    add_reference :transactions, :account, foreign_key: true
    add_reference :transactions, :user, foreign_key: true
    add_reference :users, :branch, foreign_key: true
  end
end
