class ChangeBankCodeToCode < ActiveRecord::Migration[5.2]
  def change
    rename_column :banks, :bank_code, :code
  end
end
