class RemoveColumnOfBankIdFromBranch < ActiveRecord::Migration[5.2]
  def change
    remove_column :branches, :bank_id
  end
end
