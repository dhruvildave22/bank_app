class ChangeAccountTypeToType < ActiveRecord::Migration[5.2]
  def change
    rename_column :accounts, :account_no, :number
    rename_column :accounts, :account_type, :type
  end
end
