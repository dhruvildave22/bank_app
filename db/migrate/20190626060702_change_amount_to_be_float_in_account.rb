class ChangeAmountToBeFloatInAccount < ActiveRecord::Migration[5.2]
  def up
    change_column :accounts, :balance, :float
  end

  def down
    change_column :accounts, :balance, :integer
  end
end
