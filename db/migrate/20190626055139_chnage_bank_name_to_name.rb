class ChnageBankNameToName < ActiveRecord::Migration[5.2]
   def up
    change_column :banks, :bank_name, :string
  end
  def down
    change_column :banks, :name, :string
  end
end
