class ChangeIntegerIntoBigint < ActiveRecord::Migration[5.2]
  def up
    change_column :branches, :phone_no, :bigint
    change_column :users, :phone_no, :bigint
  end
  def down
    change_column :branches, :phone_no, :integer
    change_column :users, :phone_no, :integer
  end
end
