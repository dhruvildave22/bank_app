class ChangeAmountColumnIntoFloat < ActiveRecord::Migration[5.2]
  def up
    change_column :transactions, :amount, :float
  end
  def down
    change_column :transactions, :amount, :integer
  end
end
