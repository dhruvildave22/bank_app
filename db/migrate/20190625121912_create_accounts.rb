class CreateAccounts < ActiveRecord::Migration[5.2]
  def change
    create_table :accounts do |t|
      t.integer :account_no
      t.integer :cif_no
      t.integer :balance
      t.string :account_type
      t.timestamps
    end
  end
end
