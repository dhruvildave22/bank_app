class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :name
      t.string :gender
      t.integer :age
      t.integer :phone_no
      t.string :email
      t.date :date_of_birth
      t.string :country
      t.string :city
      t.integer :zip_code
      t.string :role
      
      t.timestamps
    end
  end
end
