class ChangeAttributesOfBranchesAndTransactons < ActiveRecord::Migration[5.2]
  def change
    rename_column :branches, :branch_code, :code
    rename_column :transactions, :t_type, :type
    rename_column :transactions, :t_date, :date
  end

 
end
