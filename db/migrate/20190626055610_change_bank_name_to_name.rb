class ChangeBankNameToName < ActiveRecord::Migration[5.2]
  def change
    rename_column :banks, :bank_name, :name
  end
end
