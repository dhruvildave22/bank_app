class CreateTransactions < ActiveRecord::Migration[5.2]
  def change
    create_table :transactions do |t|
      t.string :t_type
      t.date :t_date
      t.integer :amount

    end
  end
end