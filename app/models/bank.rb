class Bank < ApplicationRecord
  has_many :branches, dependent: :destroy
  validates :name, :code, presence: true, uniqueness: true
end