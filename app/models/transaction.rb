class Transaction < ApplicationRecord
  belongs_to :account
  belongs_to :user

  validates :type, :date, :amount, :account_id, :user_id, presence: true
  validates :type, inclusion: { in: %w(deposit withdraw) }
  validates :amount, numericality: true
end