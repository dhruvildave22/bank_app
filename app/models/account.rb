class Account < ApplicationRecord
  belongs_to :branch
  belongs_to :user
  has_many :transactions

  validates :number, :type, :balance, :cif_no, :branch_id, :user_id, presence: true
  validates :number, :cif_no, uniqueness: true, numericality: true
  validates :balance, numericality: true
  validates :type, inclusion: { in: %w(saving current) }
end