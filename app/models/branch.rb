class Branch < ApplicationRecord
  belongs_to :bank
  has_many :users
  has_many :accounts

  validates :code, :location, :ifsc_code, :phone_no, :bank_id, presence: true
  validates :code, :ifsc_code, uniqueness: true
  validates :phone_no, numericality: true, length: { :minimum => 10, :maximum => 15 }
end