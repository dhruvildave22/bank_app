class User < ApplicationRecord
  belongs_to :branch, dependent: :destroy
  has_one :account
  has_many :transactions

  validates :name, :gender, :age, :phone_no, :email ,:date_of_birth , :country, :city, :zip_code, :role, presence: true
  validates :age, :phone_no, :zip_code, numericality: { only_integer: true }
  validates :email, uniqueness: true
  validates :gender, inclusion: { in: %w(male female other) }
  validates :role, inclusion: { in: %w(customer manager) }
end